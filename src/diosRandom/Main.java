package diosRandom;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;

import javax.swing.JTextArea;
import java.awt.Toolkit;
import javax.swing.JToolBar;
import javax.swing.JPopupMenu;

public class Main {

	private JFrame frmGeneradorDeDioses;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmGeneradorDeDioses.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGeneradorDeDioses = new JFrame();
		frmGeneradorDeDioses
				.setIconImage(Toolkit.getDefaultToolkit().getImage(Main.class.getResource("/diosRandom/icono.png")));
		frmGeneradorDeDioses.setTitle("Generador de dioses");
		frmGeneradorDeDioses.getContentPane().setBackground(Color.WHITE);
		frmGeneradorDeDioses.getContentPane().setForeground(Color.WHITE);
		frmGeneradorDeDioses.setBounds(100, 100, 450, 300);
		frmGeneradorDeDioses.setResizable(false);
		frmGeneradorDeDioses.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGeneradorDeDioses.getContentPane().setLayout(null);

		JLabel titulo = new JLabel("<html><u>GENERADOR DE DIOSES</u></html>");
		titulo.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 17));
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setBounds(0, 11, 434, 37);
		frmGeneradorDeDioses.getContentPane().add(titulo);

		JLabel Dios = new JLabel("");
		Dios.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		Dios.setHorizontalAlignment(SwingConstants.CENTER);
		Dios.setBounds(10, 50, 414, 23);
		frmGeneradorDeDioses.getContentPane().add(Dios);

		JTextArea item1 = new JTextArea();
		item1.setFont(new Font("Calibri", Font.PLAIN, 14));
		item1.setEditable(false);
		item1.setBounds(-67, 84, 296, 139);
		frmGeneradorDeDioses.getContentPane().add(item1);

		JLabel copyright = new JLabel("<html><u>Desarrollado por DIEGO LITTLELION | Open Source</u></html>");
		copyright.setHorizontalAlignment(SwingConstants.CENTER);
		copyright.setFont(new Font("DejaVu Sans", Font.PLAIN, 11));
		copyright.setBounds(10, 238, 304, 14);
		frmGeneradorDeDioses.getContentPane().add(copyright);

		JTextArea reliquia1 = new JTextArea();
		reliquia1.setFont(new Font("Calibri", Font.PLAIN, 14));
		reliquia1.setEditable(false);
		reliquia1.setBounds(285, 84, 194, 139);
		frmGeneradorDeDioses.getContentPane().add(reliquia1);

		JButton Generar = new JButton("Generar");
		Generar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					int diosRandom;
					int itemsRandom, contador = 1;
					int rol = (int) (Math.random() * 3);
					int reliquiaRandom;
					String zapato;
					String item;
					String reliquia;

					/* Reliquias */
					String reliquias[] = { "Heavenly Wings", "Cursed Ankh", "Aegis Amulet", "Blink Rune",
							"Purification Beads", "Teleport Glyph", "Meditation Cloak", "Magic Shell",
							"Shield of Thorns", "Sundering Spear", "Phantom Veil", "Horrific Emblem",
							"Bracer of Undoing", "Belt of Frenzy" };

					/* TODO LO DE MAGOS */
					String magicos[] = { "Merlin", "Ra", "Artio", "The Morrigan", "Ao Kuang", "Chang'e", " He Bo",
							"Nu Wa", "Xing Tian", "Zhong Kui", "Geb", "Isis", "Khepri", "Anubis", "Sobek", "Thoth",
							"Aphrodite", "Ares", "Athena", "Cerberus", "Chronos", "Hades", "Hera", "Poseidon", "Scylla",
							"Zeus", "Agni", "Ganesha", "Kumbhakarna", "Kuzenbo", "Raijin", "Kukulkan", "Ah Puch",
							"Cabrakan", "Fafnir", "Freya", "Hel", "Sol", "Ymir", "Bacchus", "Discordia", "Janus", "Nox",
							"Sylvanus", "Terra", "Vulcan", "Baron Samedi" };

					String itemsMagicos[] = { "Dynasty Plate Helm", "Stone of Binding", "Winged Blade", "Relic Dagger",
							"Shiel of Regrowth", "Emperor's Armor", "Celestial Legion Helm", "Witchblade",
							"Lotus Crown", "Sovereignty", "Oni Hunter's Garb", "Heartward Amulet", "Spectral Armor",
							"Demonic Grip", "Void Stone", "Jade Emperor's Crown", "Magi's Cloak", "Talisman of Energy",
							"Genji's Guard", "Hide of the Nemean Lion", "Shogun's Kusari", "Toxic Blade",
							"Mail of Renewal", "Pestilence", "Stone of Gaia", "Lono's Mask", "Divine Ruin",
							"Pythagorem's Piece", "Breastplate of Valor", "Polynomicon", "Soul Gem",
							"Spear of the Magus", "Stone of Fal", "Hastened Ring", "Midgardian Mail", "Obsidian Shard",
							"Bulwark of Hope", "Gauntlet of Thebes", "Shaman's Ring", "Hide of the Urchin",
							"Bumba's Mask", "Spirit Robe", "Bancroft's Talon", "Ethereal Staff", "Rod of Asclepius",
							"Spear of Desolation", "Book of the Dead", "Book of Thoth", "Warlock's Staff",
							"Gem of Isolation", "Mystical Mail", "Telkhines Ring", "Soul Reaver", "Chrono's Pendant",
							"Typhon's Fang", "Mantle of Discord", "Rod of Tahuti", "Doom Orb" };

					String botasMagicas[] = { "Shoes of the Magi", "Shoes of Focus", "Reinforced Shoes",
							"Traveler's Shoes" };

					/* TODO LO DE FISICOS */
					String fisicos[] = { "Arthur King", "Cu Chulainn", "Da ji", "Erlang Shen", "Guan Yu", "Ne Zha",
							"Sun Wukong", "Bastet", "Osiris", "Serqet", "Achilles", "Arachne", "Nemesis", "Nike",
							"Thanatos", "Bakasura", "Kali", "Ravana", "Vamana", "Amaterasu", "Susano", "Awilix",
							"Camazotz", "Chaac", "Hun Batz", "Fenrir", "Loki", "Odin", "Ratatoskr", "Thor", "Tyr",
							"Pele", "Bellona", "Hercules", "Mercury" };

					String botasFisicas[] = { "Warrior Tabi", "Ninja Tabi", "Reinforced Greaves", "Talaria Boots" };

					String itemsFisicos[] = { "Berserker's Shield", "Asi", "Gladiator's Shield", "Ichaival",
							"Winged Blade", "Relic Dagger", "Shield of Regrowth", "Blackthorn Hammer",
							"Emperor's Armor", "Witchblade", "Odysseus' Bow", "Sovereignty", "Oni Hunter's Garb",
							"Heartward Amulet", "Spectral Armor", "Runic Shield", "Ancile", "Hydra's Lament",
							"Magi's Cloak", "Talisman of Energy", "Genji's Guard", "Hide of the Nemean Lion",
							"Atalanta's Bow", "Shogun's Kusari", "Silverbranch Bow", "Golden Blade", "Hastened Katana",
							"Toxic Blade", "Mail of Renewal", "Pestilence", "Stone of Gaia", "Devourer's Gauntlet",
							"Frostbound Hammer", "Breastplate of Valor", "Runeforged Hammer", "Heartseeker",
							"Midgardian Mail", "Soul Eater", "Titan's Bane", "Brawler's Beat Stick", "The Executioner",
							"Jotunn's Wrath", "Bulwark of Hope", "Shifter's Shield", "Rage", "Gauntlet of Thebes",
							"The Crusher", "Hide of the Urchin", "Bumba's Mask", "Rangda's Mask", "Masamune",
							"Spirit Robe", "Poisoned Star", "Stone Cutting Sword", "Transcendence", "Wind Demon",
							"Void Shield", "Mystical Mail", "Qin's Sais", "Bloodforge", "Mantle of Discord",
							"Deathbringer", "Malice" };

					/* TODO LO DE ADC */
					String fisicosAdc[] = { "Hou Yi", "Jing Wei", "Anhur", "Neith", "Chernobog", "Apolo", "Artemisa",
							"Medusa", "Quiron", "Rama", "Izanami", "Hachiman", "Ah Muzen Cab", "Xbalanque", "Cupido",
							"Skadi", "Ullr", "Cernunnos" };

					String botasFisicasAdc[] = { "Warrior Tabi", "Ninja Tabi", "Reinforced Greaves", "Talaria Boots" };

					String itemsFisicosAdc[] = { "Berserker's Shield", "Asi", "Gladiator's Shield", "Ichaival",
							"Winged Blade", "Relic Dagger", "Shield of Regrowth", "Blackthorn Hammer",
							"Emperor's Armor", "Witchblade", "Odysseus' Bow", "Sovereignty", "Oni Hunter's Garb",
							"Heartward Amulet", "Spectral Armor", "Runic Shield", "Ancile", "Hydra's Lament",
							"Magi's Cloak", "Talisman of Energy", "Genji's Guard", "Hide of the Nemean Lion",
							"Atalanta's Bow", "Shogun's Kusari", "Silverbranch Bow", "Toxic Blade", "Mail of Renewal",
							"Pestilence", "Sotne of Gaia", "Lono's Mask", "Devourer's Gauntlet", "Frostbound Hammer",
							"Breastplate of Valor", "Runeforged Hammer", "Heartseeker", "Midgardian Mail", "Soul Eater",
							"Titan's Bane", "Brawler's Beat Stick", "The Executioner", "Jotunn's Wrath",
							"Bulwark of Hope", "Shifter's Shield", "Rage", "Gauntlet of Thebes", "The Crusher",
							"Hide of the Urchin", "Bumba's Mask", "Spirit Robe", "Poisoned Star", "Transcendence",
							"Wind Demon", "Void Shield", "Mystical Mail", "Qin's Sais", "Bloodforge",
							"Mantle of Discord", "Deathbringer", "Malice" };

					String buildFinal[] = new String[6];
					String reliquiasFinales[] = new String[2];

					switch (rol) {
					/* Mago */
					case 0:
						diosRandom = (int) (Math.random() * magicos.length);
						Dios.setText("Dios: " + magicos[diosRandom]);
						itemsRandom = (int) (Math.random() * 4);
						zapato = botasMagicas[itemsRandom];
						buildFinal[0] = zapato;
						while (contador <= 5) {
							itemsRandom = (int) (Math.random() * itemsMagicos.length);
							item = itemsMagicos[itemsRandom];
							while (Arrays.stream(buildFinal).anyMatch(item::equals)) {
								itemsRandom = (int) (Math.random() * itemsMagicos.length);
								item = itemsMagicos[itemsRandom];
							}
							buildFinal[contador] = item;
							contador++;
						}
						break;

					/* F�sico */
					case 1:
						diosRandom = (int) (Math.random() * fisicos.length);
						Dios.setText("Dios: " + fisicos[diosRandom]);
						itemsRandom = (int) (Math.random() * 4);
						zapato = botasFisicas[itemsRandom];
						buildFinal[0] = zapato;
						while (contador <= 5) {
							itemsRandom = (int) (Math.random() * itemsFisicos.length);
							item = itemsFisicos[itemsRandom];
							while (Arrays.stream(buildFinal).anyMatch(item::equals)) {
								itemsRandom = (int) (Math.random() * itemsFisicos.length);
								item = itemsFisicos[itemsRandom];
							}
							buildFinal[contador] = item;
							contador++;
						}
						break;

					/* Fisico ADC */
					case 2:
						diosRandom = (int) (Math.random() * fisicosAdc.length);
						Dios.setText("Dios: " + fisicosAdc[diosRandom]);
						itemsRandom = (int) (Math.random() * 4);
						zapato = botasFisicasAdc[itemsRandom];
						buildFinal[0] = zapato;
						while (contador <= 5) {
							itemsRandom = (int) (Math.random() * itemsFisicosAdc.length);
							item = itemsFisicosAdc[itemsRandom];
							while (Arrays.stream(buildFinal).anyMatch(item::equals)) {
								itemsRandom = (int) (Math.random() * itemsFisicosAdc.length);
								item = itemsFisicosAdc[itemsRandom];
							}
							buildFinal[contador] = item;
							contador++;
						}
						break;
					}

					/* Generamos las reliquias */
					contador = 0;
					while (contador < 2) {
						reliquiaRandom = (int) (Math.random() * 14);
						reliquia = reliquias[reliquiaRandom];
						while (Arrays.stream(reliquiasFinales).anyMatch(reliquia::equals)) {
							reliquiaRandom = (int) (Math.random() * 14);
							reliquia = reliquiasFinales[reliquiaRandom];
						}
						reliquiasFinales[contador] = reliquia;
						contador++;
					}

					/* Muestra la build final */
					item1.setText("  	Items:");
					for (int j = 0; j < buildFinal.length; j++) {
						item1.setText(item1.getText() + "\n	" + (j + 1) + ".  " + buildFinal[j]);
					}

					/* Mostramos las reliquias */
					reliquia1.setText("  Reliquias:");
					for (int j = 0; j < reliquiasFinales.length; j++) {
						reliquia1.setText(reliquia1.getText() + "\n" + (j + 1) + ".  " + reliquiasFinales[j]);
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					/*JOptionPane.showMessageDialog(frmGeneradorDeDioses, "Procura no clickar tan r�pido, impaciente :v",
							"Warning", JOptionPane.WARNING_MESSAGE);*/
				}
			}
		});
		Generar.setBounds(335, 234, 89, 23);
		frmGeneradorDeDioses.getContentPane().add(Generar);
	}
}
